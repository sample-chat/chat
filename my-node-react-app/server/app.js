const express = require('express');

const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require("cors");
const app = express();

const { Server } = require("socket.io");
app.use(cors());

const http = require('http').createServer(app);

const io = new Server(http, {
  cors: {
    origin: "http://localhost:3000",
    methods: ["GET", "POST"],
  },
});
// const routes = require('./routes');


const userRoutes = require('./routes/userRoutes');
const messagesRoutes = require('./routes/messagesRoutes');
const contactRoutes = require('./routes/contactRoutes');
const chatRoutes = require('./routes/chatRoutes');
const emojiRoutes = require('./routes/emojiRoutes');
const fileRoutes = require('./routes/fileRoutes');
const groupChatRoutes = require('./routes/groupChatRoutes');
const notificationRoutes = require('./routes/notificationRoutes');
const onlineStatusRoutes = require('./routes/onlineStatusRoutes');
const wysiwygRoutes = require('./routes/wysiwygRoutes');






// Connect to the MongoDB database
mongoose.connect('mongodb+srv://admin:admin123@zuitt-bootcamp.tr9npll.mongodb.net/VCC?retryWrites=true&w=majority', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

// Event listeners for connection status
mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas.'));

// Middleware
app.use(express.json());
app.use(cors());
app.use(bodyParser.json());
app.use(express.urlencoded({extended:true}));




// Socket.IO configuration
io.on('connection', (socket) => {
    console.log(`User Connected: ${socket.id}`);
  
    

    socket.on("join_room", (data) => {
      socket.join(data);
      console.log(`User with ID: ${socket.id} joined room: ${data}`);
      console.log(`ROOM IS AVAILABLE`);
    });

    socket.on("send_message", (data) => {
      socket.to(data.room).emit("receive_message", data);
    });

    socket.on('newMessage', (message) => {
      if (!message.isLoggedIntoRoom) {
        handleSendMessage(message.contact);
      }
    });
  
    socket.on('disconnect', () => {
      console.log('A user disconnected');
    });
});

// Routes
app.use('/api/user', userRoutes);
app.use('/api/message', messagesRoutes);
app.use('/api/contact', contactRoutes);
app.use('/api/chat', chatRoutes);
app.use('/api/emoji', emojiRoutes);
app.use('/api/file', fileRoutes);
app.use('/api/groupchat', groupChatRoutes);
app.use('/api/notification', notificationRoutes);
app.use('/api/onlinestatus', onlineStatusRoutes);
app.use('/api/wysiwyg', wysiwygRoutes);



// Start the server
const PORT = process.env.PORT || 5000;
http.listen(PORT, () => {
  console.log(`API is now online on port ${PORT}`);
});
