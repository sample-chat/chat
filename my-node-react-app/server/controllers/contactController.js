const Contact = require('../models/Contact'); // Assuming you have a Contact model
const User = require('../models/User');


// Get Contacts 

module.exports.getContacts = async (userId) => {
  try {
    // Find the user by ID
    const user = await User.findById(userId);
    if (!user) {
      throw new Error('User not found');
    }

    // Find all contacts associated with the user
    const contacts = await Contact.find({ user: userId });

    return contacts;
  } catch (error) {
    throw new Error('Error fetching contacts');
  }
};



// Get Contacts Details

module.exports.getContactDetails = async (contactId) => {
  try {
    // Find the user by ID
    const contact = await User.findById(contactId);
    if (!contact) {
      throw new Error('Contact not found');
    }

    return contact;
  } catch (error) {
    throw new Error('Error fetching contacts');
  }
};

// Get Contacts Details

module.exports.getContactDetailsByName = async (contactName) => {
  try {
    // Find the user by ID
    const contact = await User.findOne({username: contactName});
    if (!contact) {
      throw new Error('Contact not found');
    }

    return contact;
  } catch (error) {
    throw new Error('Error fetching contacts');
  }
};

// ADD Contact

module.exports.addContact = async (userId, contactName, contactEmail) => {
  try {
    
    // Find the user by ID
    const user = await User.findById(userId);
    const contact = await User.findOne({ username: contactName });

    if (!user) {
      throw new Error('User not found');
    }

    // Check if the contact already exists by email
    const existingContact = await Contact.findOne({ email: contactEmail });

    console.log("contactEmail: " + contactEmail)


    if (existingContact) {
      throw new Error('Contact already exists');
    }

    // Create a new contact
    const newContact = await Contact.create({
      user: userId,
      contactId: contact._id
    });

    return newContact;
  } catch (error) {
    throw new Error('Error adding contact');
  }
};

// Delete Contact

module.exports.deleteContact = async (userId, contactId) => {
  try {

    const contact = await Contact.findOne({
      user: userId,
      contactId: contactId
    });

    const itemId = contact._id;

    console.log("this is the user acc: " + contact);

    if (!contact) {
      throw new Error('User Account not found');
    }

    console.log("this is the contactID: " + contactId)
    console.log("this is the contact._id: " + itemId)

    // Remove the contact by its ID
    await Contact.findByIdAndRemove(itemId);

    console.log("LAST");
  } catch (error) {
    throw new Error('An error occurred while removing the contact');
  }
};
