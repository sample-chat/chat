const OnlineStatus = require('../models/OnlineStatus'); // Assuming you have an OnlineStatus model

module.exports.updateOnlineStatus = async (userId, isOnline) => {
  try {
    let updatedStatus;

    if (isOnline) {
      updatedStatus = await OnlineStatus.findOneAndUpdate(
        { user: userId },
        { isOnline },
        { new: true, upsert: true }
      );
    } else {
      const onlineStatus = await OnlineStatus.findOne({ user: userId });

      if (onlineStatus) {
        // Update the lastOnline field only if the user was previously online
        updatedStatus = await OnlineStatus.findOneAndUpdate(
          { user: userId },
          { isOnline, lastOnline: new Date() },
          { new: true, upsert: true }
        );
      }
    }

    return updatedStatus;
  } catch (error) {
    throw new Error('An error occurred while updating online status');
  }
};

module.exports.getOnlineStatusOfContacts = async (userId) => {
  try {

    let onlineStatusList = await OnlineStatus.findOne({ user: userId });

    if (!onlineStatusList) {
      // If onlineStatusList is undefined or null, create a new OnlineStatus record
      onlineStatusList = await OnlineStatus.create({ user: userId, isOnline: false });
    }

    return onlineStatusList;
  } catch (error) {
    throw new Error('An error occurred while retrieving online status of contacts');
  }
};
