const GroupChat = require('../models/GroupChat'); // Assuming you have a GroupChat model

module.exports.createGroupChat = async (name, members) => {
  try {
    const newGroupChat = new GroupChat({
      name,
      members,
      messages: [],
    });

    const createdGroupChat = await newGroupChat.save();
    return createdGroupChat;
  } catch (error) {
    throw new Error('An error occurred while creating the group chat');
  }
};

module.exports.addMembersToGroupChat = async (groupId, newMembers) => {
  try {
    const updatedGroupChat = await GroupChat.findByIdAndUpdate(
      groupId,
      { $addToSet: { members: { $each: newMembers } } },
      { new: true }
    );

    return updatedGroupChat;
  } catch (error) {
    throw new Error('An error occurred while adding members to the group chat');
  }
};

module.exports.sendGroupChatMessage = async (groupId, senderId, message) => {
  try {
    const updatedGroupChat = await GroupChat.findByIdAndUpdate(
      groupId,
      {
        $push: {
          messages: {
            senderId,
            message,
            createdAt: new Date(),
          },
        },
      },
      { new: true }
    );

    return updatedGroupChat;
  } catch (error) {
    throw new Error('An error occurred while sending a message in the group chat');
  }
};

module.exports.getGroupChatHistory = async (groupId) => {
  try {
    const groupChat = await GroupChat.findById(groupId);
    return groupChat.messages;
  } catch (error) {
    throw new Error('An error occurred while retrieving group chat history');
  }
};

module.exports.deleteGroupChat = async (groupId) => {
  try {
    await GroupChat.findByIdAndDelete(groupId);
  } catch (error) {
    throw new Error('An error occurred while deleting the group chat');
  }
};
