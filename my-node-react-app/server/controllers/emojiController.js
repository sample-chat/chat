const Emoji = require('../models/Emoji'); // Assuming you have an Emoji model

module.exports.getAvailableEmojis = async () => {
  try {
    const emojis = await Emoji.find();

    return emojis;
  } catch (error) {
    throw new Error('An error occurred while retrieving available emojis');
  }
};
