const Notification = require('../models/Notifications'); // Assuming you have a Notification model
const io = require('../socket'); // Assuming you have set up socket.io for real-time communication

module.exports.sendNotification = async (userId, message) => {
  try {
    const newNotification = await Notification.create({ userId, message });

    // Emit the new notification to the user using socket.io
    io.getIO().to(userId).emit('notification', newNotification);

    return newNotification;
  } catch (error) {
    throw new Error('An error occurred while sending the notification');
  }
};

module.exports.markNotificationAsRead = async (notificationId) => {
  try {
    const notification = await Notification.findByIdAndUpdate(notificationId, { read: true });

    if (!notification) {
      throw new Error('Notification not found');
    }

    return notification;
  } catch (error) {
    throw new Error('An error occurred while marking the notification as read');
  }
};
