const Chat = require('../models/ChatRoom'); // Assuming you have a Chat model
const Message = require('../models/Messages'); // Assuming you have a Message model

module.exports.startChat = async (participants) => {
  try {
    const newChat = await Chat.create({ participants });
    return newChat;
  } catch (error) {
    throw new Error('An error occurred while starting a new chat');
  }
};

module.exports.sendMessage = async (userId, contactId, content) => {
  try {
    // Create the message
    const newMessage = await Message.create({
      senderId: userId,
      receiverId: contactId,
      content: content
    });

    // Create or find the chat
    let chat = await Chat.findOne({
      participants: [userId, contactId]
    });

    if (!chat) {
      // If the chat doesn't exist, create it
      chat = await Chat.create({
        participants: [userId, contactId],
        messages: [newMessage._id] // Add the message reference to the chat's messages
      });
    } else {
      // If the chat exists, add the message reference to its messages
      chat.messages.push(newMessage._id);
      await chat.save();
    }

    return newMessage;
  } catch (error) {
    throw new Error('An error occurred while sending a message');
  }
};



module.exports.getChatHistory = async (userId) => {
  try {
    // Find all chats where the user is a participant
    const chatHistory = await Chat.find({
      participants: userId
    }).populate('participants', 'username'); // Populate participants' usernames

    return chatHistory;
  } catch (error) {
    throw new Error('An error occurred while fetching chat history');
  }
};



// module.exports.markMessagesAsRead = async (req, res) => {
//   try {
//     const { chatId, userId } = req.body;

//     await Message.updateMany({ chatId, sender: { $ne: userId } }, { $set: { read: true } });

//     res.status(200).json({ message: 'Messages marked as read' });
//   } catch (error) {
//     throw new Error('An error occurred');
//   }
// };

// module.exports.deleteChat = async (req, res) => {
//   try {
//     const chatId = req.params.id;

//     await Chat.findByIdAndDelete(chatId);

//     res.status(200).json({ message: 'Chat deleted successfully' });
//   } catch (error) {
//     throw new Error('An error occurred');
//   }
// };
