const socketIo = require('socket.io');

let io;

module.exports = {
  init: (server) => {
    io = socketIo(server);
    return io;
  },
  getIo: () => {
    if (!io) {
      throw new Error('Socket.io not initialized!');
    }
    return io;
  },
  emitToUser: (userId, event, data) => {
    io.to(userId).emit(event, data);
  },
};
