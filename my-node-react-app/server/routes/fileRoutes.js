const express = require('express');
const router = express.Router();
const fileController = require('../controllers/fileController');

// Route to upload a file
router.post('/upload', async (req, res) => {
  try {
    const uploadedFile = await fileController.uploadFile(req.file); // Assuming you're using multer for file uploads
    res.status(201).json(uploadedFile);
  } catch (error) {
    res.status(500).json({ error: 'An error occurred while uploading the file' });
  }
});

// Route to retrieve uploaded files
router.get('/files', async (req, res) => {
  try {
    const files = await fileController.getUploadedFiles();
    res.status(200).json(files);
  } catch (error) {
    res.status(500).json({ error: 'An error occurred while fetching uploaded files' });
  }
});

module.exports = router;
