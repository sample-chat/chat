const express = require('express'); 
const router = express.Router();
const onlinestatusController = require('../controllers/onlinestatusController');

// Route to update user's online status
router.put('/update', async (req, res) => {

  const { userId, isOnline } = req.body;
  try {
    const updatedStatus = await onlinestatusController.updateOnlineStatus(userId,isOnline);
    res.status(200).json(updatedStatus);
  } catch (error) {
    res.status(500).json({ error: 'An error occurred while updating the online status' });
  }
});

// Route to retrieve online status of contacts
router.get('/online/:userId', async (req, res) => {
  try {
    const onlineStatuses = await onlinestatusController.getOnlineStatusOfContacts(req.params.userId);
    res.status(200).json(onlineStatuses);
  } catch (error) {
    res.status(500).json({ error: 'An error occurred while retrieving online statuses of contacts' });
  }
});

module.exports = router;
