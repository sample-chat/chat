const express = require('express');
const router = express.Router();
const Notification = require('../models/Notifications');
const socket = require('../socket'); // Import socket.io instance

// Send real-time notifications to users
router.post('/send', async (req, res) => {
  try {
    // Create and save a new notification
    const { userId, message } = req.body;
    const newNotification = new Notification({ userId, message });
    const savedNotification = await newNotification.save();

    // Emit the new notification to the user using socket
    socket.emitToUser(userId, 'notification', savedNotification);

    res.status(201).json(savedNotification);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

// Mark notifications as read
router.put('/:id/mark-read', async (req, res) => {
  try {
    const notificationId = req.params.id;

    // Update the notification to mark it as read
    const updatedNotification = await Notification.findByIdAndUpdate(
      notificationId,
      { read: true },
      { new: true }
    );

    res.json(updatedNotification);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

module.exports = router;
