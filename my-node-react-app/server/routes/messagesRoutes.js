const express = require('express');
const router = express.Router();
const messagesController = require('../controllers/messagesController');

// Route to send a new message
router.post('/send', async (req, res) => {
  
  
  try {
    const newMessage = await messagesController.sendMessage(req.body);
    console.log(newMessage)
    res.status(201).json(newMessage);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

// Route to get chat history between two users
router.get('/history/:user1Id/:user2Id', async (req, res) => {
  const { user1Id, user2Id } = req.params;
  try {
    const chatHistory = await messagesController.getChatHistory(user1Id, user2Id);
    res.status(200).json(chatHistory);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

// Route to get chat history between two users 2nd option
router.post('/history', async (req, res) => {
  const { user1Id, contact } = req.body;
  
  try {
    const chatHistory = await messagesController.getChatHistoryWithContactUsername(user1Id, contact);
    res.status(200).json(chatHistory);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

// Other message-related routes (e.g., markMessageAsRead, deleteMessage, etc.)

module.exports = router;
