const express = require('express');
const router = express.Router();
const chatController = require('../controllers/chatController');

// Route to start a new one-on-one chat
router.post('/start', async (req, res) => {
  try {
    const { userId, contactId } = req.body;
    const participants = [userId, contactId];

    const newChat = await chatController.startChat(participants);

    res.status(201).json(newChat);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

// Route to send a message in a chat
router.get('/history/:userId', async (req, res) => {
  const { userId } = req.params;
  try {
    const chatHistory = await chatController.getChatHistory(userId);
    res.status(200).json(chatHistory);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});


// Route to get chat history
router.get('/history/:chatId', async (req, res) => {
  const { chatId } = req.params;
  try {
    const chatHistory = await chatController.getChatHistory(chatId);
    res.status(200).json(chatHistory);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

// Route to mark messages as read
// router.put('/mark-read/:chatId', async (req, res) => {
//   const { chatId } = req.params;
//   try {
//     await chatController.markMessagesAsRead(chatId);
//     res.status(200).json({ message: 'Messages marked as read' });
//   } catch (error) {
//     res.status(500).json({ error: 'An error occurred while marking messages as read' });
//   }
// });

// Route to delete a chat
// router.delete('/delete/:chatId', async (req, res) => {
//   const { chatId } = req.params;
//   try {
//     await chatController.deleteChat(chatId);
//     res.status(200).json({ message: 'Chat deleted successfully' });
//   } catch (error) {
//     res.status(500).json({ error: 'An error occurred while deleting the chat' });
//   }
// });

module.exports = router;
