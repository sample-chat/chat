const express = require('express');
const router = express.Router();
const emojiController = require('../controllers/emojiController');

// Route to retrieve available emojis
router.get('/emojis', async (req, res) => {
  try {
    const emojis = await emojiController.getAvailableEmojis();
    res.status(200).json(emojis);
  } catch (error) {
    res.status(500).json({ error: 'An error occurred while fetching emojis' });
  }
});

module.exports = router;
