// models/Notification.js
const mongoose = require('mongoose');

const notificationSchema = new mongoose.Schema({
  recipient: { 
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'User', 
    required: true 
},
  type: { 
    type: String, 
    required: true 
},
  content: { 
    type: String, 
    required: true 
},
  timestamp: { 
    type: Date, 
    default: Date.now 
},
  // Add other fields as needed
});

const Notification = mongoose.model('Notification', notificationSchema);

module.exports = Notification;
