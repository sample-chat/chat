const mongoose = require('mongoose');

const wysiwygSchema = new mongoose.Schema({
  content: {
    type: String,
    required: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

const Wysiwyg = mongoose.model('Wysiwyg', wysiwygSchema);

module.exports = Wysiwyg;
