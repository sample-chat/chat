// models/OnlineStatus.js
const mongoose = require('mongoose'); 

const onlineStatusSchema = new mongoose.Schema({
  user: { 
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'User', required: true 
},
  isOnline: { 
    type: Boolean, 
    default: false 
},
  lastOnline: { 
    type: Date 
},
  // Add other fields as needed
});

const OnlineStatus = mongoose.model('OnlineStatus', onlineStatusSchema);

module.exports = OnlineStatus;
