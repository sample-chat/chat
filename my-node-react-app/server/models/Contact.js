// models/Contact.js
const mongoose = require('mongoose');

const contactSchema = new mongoose.Schema({
  
    user: { 
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'User', 
        required: true 
    },
    contactId: { 
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'User', 
        required: true 
    },
  // Add other fields as needed
});

const Contact = mongoose.model('Contact', contactSchema);

module.exports = Contact;
